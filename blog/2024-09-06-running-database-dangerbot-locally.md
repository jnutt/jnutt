# Running the database review locally

Dangerbot is configured to skip database checks except in CI. If you're trying to figure out which changes Dangerbot is flagging for review, you can update the Dangerfile and run Dangerbot locally.

```diff
diff --git a/danger/database/Dangerfile b/danger/database/Dangerfile
index b7f2151dcf9b..0a679baa522f 100644
--- a/danger/database/Dangerfile
+++ b/danger/database/Dangerfile
@@ -68,7 +68,6 @@ if geo_migration_created && !geo_db_schema_updated
   warn format(format_str, migrations: 'Geo migrations', schema: helper.html_link("ee/db/geo/structure.sql"))
 end
 
-return unless helper.ci?
 return if helper.mr_labels.include?(DATABASE_APPROVED_LABEL)
 
 migration_testing_has_run = helper.mr_labels.include?(DATABASE_TESTING_RUN_LABEL)
```

```bash
bin/rake danger_local
```
