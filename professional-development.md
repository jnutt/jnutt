# Professional development

[IGP (private link)](https://docs.google.com/document/d/1iy1J1J3uVQ_tKdO1SKxLTcQ1TQi-NkZi6OIqST7E7QQ/edit)

## Bragging

This is a slightly abridged version of my personal brag doc. It's more concrete, and leaves out the private gushing.

### 2023

- 2023-09-13: First MR merged.
- 2023-10-16: Community contribution "high performer."
- 2023-10-19: Finished [Sidekiq in Practice](https://nateberk.gumroad.com/l/sidekiqinpractice).

### 2024

- 2024-01: Acted as on-boarding buddy for Ivane.
- 2024-01-30: Passed probation.
- 2024-03: Acting as DRI for technical OKR.
- 2024-04-04: Became a backend maintainer.
- 2024-04-29: Community contribution "top performer."
- 2024-06: EMEA DRI for SIRT response (briefly).
- 2024-06-12: Handed out GitLab socks at local tech event.
- 2024-08-10: Finished [Effective Testing with RSpec 3](https://pragprog.com/titles/rspec3/effective-testing-with-rspec-3/).
- 2024-09: Acting as DRI for process OKR.

## Learning objectives

- Gain strong level of proficiency with GraphQL.
  - [ ] Read & understand [GraphQL style guide](https://docs.gitlab.com/ee/development/api_graphql_styleguide.html).
- [ ] Read [The Software Engineer's Guidebook](https://www.engguidebook.com/).
