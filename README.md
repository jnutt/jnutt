# About me

I'm a Senior Backend Engineer on the [Import & Integrate team](https://about.gitlab.com/handbook/engineering/development/dev/manage/import-and-integrate/). Together, we work on the GitLab API, importing & exporting data, and integrating with third party systems.

- [/uses](https://jsrn.net/uses)
- [/stuffthatilike](https://jsrn.net/stuffthatilike)

Please send me pictures of your dog.

## Working with me

I prefer to work async as much as possible. It gives me the space to effectively organise and present my thoughts. That said, I'm always happy to jump on a call to say hi or chat in DMs.

I try to guard my focus while remaining accessible. I'm experimenting with keeping Slack and e-mail closed and checking them at set times or between tasks.

## Current foci

- Learning as much as I can. I'm just about settled in, but every day still brings a lot of new things.
- Furthering my team's OKRs.
- Brevity. My natural writing style can be a bit [purple](https://en.wikipedia.org/wiki/Purple_prose). I'm working to [tighten it up](https://jsrn.net/2024/03/04/trying-out-vale.html). If I come across as short, I'm sorry!
