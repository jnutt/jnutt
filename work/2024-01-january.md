# January 2024

## Merge requests

- [Fix broken link in Rubocop message](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141077)
- [Rename StuckImportWorker to StaleImportWorker](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/139676#note_1736396735)
- [API endpoint for re-import of named relation from project export file"](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142186)
- [Remove validate_import_decompressed_archive_size feature flag](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142688)
- [Allow non-development environments to perform local project imports](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142835)
- [Pass outbound_local_requests_whitelist to ProjectImporter](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142893)

## Reviews

- [Remove feature flag allow_streaming_instance_audit_events_to_amazon_s3 ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140774)
- [Modify UX for dependency proxy project settings form](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140339)
- [Refactor elasticsearch task status for reindexing](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140261)
- [RSpec DNS helper to permit Vite connections ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141144)
- [Jira Issues Integration: Error 500 when Jira host is unreachable ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141363)
- [Fix anchor in pre-receive secret detection documentation link ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141722)
- [REST API support for GitLab for Slack app integration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141693)
- [Remove bitbucket_server_importer_exponential_backoff feature flag ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140175)
- [Remove bitbucket_importer_exponential_backoff feature flag ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140041)
- [Enable setting up Jira issues integration on the group level ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141996)
- [Allow non-admin access to user activities ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141767)
- [Show the error message when event definition is missing for event ✨💡](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142200)
  - TIL `Gitlab.Gitlab.dev_or_test_env?`
- [Calculate usage limits for PA and expose to graphql API](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142007)
- [Update Quick Action test to be less relient on the front end](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142236)
- [Don't send email when importing members using DirectTransfer ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142546)
- [Fix issue search when created from another locale ✨💡](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142652)
  - It ended up being okay, but the maintainer sparked an interesting discussion as to whether `Gitlab::I18n.with_default_locale` is thread safe, which I hadn't considered.
- [Fallback to ILIKE search for group work items ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142888)
- [Change purchase logic for product analytics ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142946)
- [Clean up caching importer 🤝✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/139395)
  - A pretty chunky MR, but the contributer had heavily collaborated with an existing GitLab team member, so it's no surprise it was in great shape by the time it got to me.

## Issues raised

- [`Gitlab::HTTP_V2::UrlAllowlist#ip_allowed?` doesn't respect `Gitlab::CurrentSettings.outbound_local_requests_whitelist`](https://gitlab.com/gitlab-org/gitlab/-/issues/439463)
- [Remove `BulkImports::StuckImportWorker` class](https://gitlab.com/gitlab-org/gitlab/-/issues/439721)

## Other

- I passed my probation! Hoorah!
