# November 2023

## MRs

- [Limit concurrent BulkImports::PipelineBatchWorker](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136018) (adopted from `@.luke`)
- [Update index name](https://gitlab.com/gitlab-org/manage/import-and-integrate/team/-/merge_requests/11)
- [Include sample curl command for testing direct transfer](https://gitlab.com/gitlab-org/manage/import-and-integrate/team/-/merge_requests/12)
- [Set failure flag on parent import when an entity fails](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138036)
- [Update termination grace period for gstg](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/3256)

## Reviews

- [Fully qualify Gitlab::Metrics to avoid code reloading issues](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136366)
- [Fixes rubocop violation in repo storage spec](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136435)
- [Security MR 🔒](https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/3685)
- [Make BulkImportWorker and BulkImport::EntityWorker retriable](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/135946)
- [Remove unused helper method](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136133)
- [Fix Sidekiq warning for Gitlab::GithubImport::\*Worker](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/135724)
- [Swap mock GraphQL query for the real in organizations list](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136506)
- [Database fixes for Rails 7.1 that can be extracted into default branch](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136003)
- [Security MR 🔒](https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/3686)
- [Remove ignore rule for jitsu_key](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136827)
- [Add a project actor to non_public_artifacts feature flag](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136488)
- [Remove the frecent_namespaces_suggestions feature flag](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137448)
- [Add new trial badge indicator](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136199)
- [Convert events.author_id to users foreign key to a LFK](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138215)

## Issues raised

- [Unable to import project into nested group using just `path' field via API](https://gitlab.com/gitlab-org/gitlab/-/issues/432805)
- [Allow users of the Direct Transfer API to skip certain relations](https://gitlab.com/gitlab-org/gitlab/-/issues/432809)

## Other

- [Stress test Direct Transfer](https://gitlab.com/gitlab-org/gitlab/-/issues/429981)
- [Requirements for all importers](https://gitlab.com/gitlab-org/gitlab/-/issues/430051)
- [Add James Nutt as Max Woolf's backend maintainer mentee](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/131524)
- [100 projects migration has some projects failing to be migrated with `Export from source instance failed: 404` error](https://gitlab.com/gitlab-org/gitlab/-/issues/432547)
