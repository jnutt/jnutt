# December 2023

## Merge requests

- [Make Direct Transfer logging more consistent](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138990)
- [Save external_identifiers on ImportFailure for file import relations](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/139653)
- [Rename StuckImportWorker to StaleImportWorker](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/139676)
- [Running gitea with podman](https://gitlab.com/gitlab-org/manage/import-and-integrate/team/-/merge_requests/15)
- [Draft: Avoid mapping by connected GitHub accounts during gitea import](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140326)
- [Avoid mapping by connected GitHub accounts during gitea import](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140439)
- [Draft: Two Sidekiq resources I've found useful](https://gitlab.com/gitlab-org/manage/import-and-integrate/team/-/merge_requests/17)

## Reviews

- [Retry exceptions during BulkImports::FileDownloadService ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138494)
- [Remove unused `MAX_DAYS_OF_HISTORY` from vulnerabilities ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138630)
- [Update Jira integration's regex field text](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138638)
- [Retry exceptions during BulkImports::FileDownloadService ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138494)
- [Issue cross-reference notes use issue type name ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/139017)
- [Remove sequential bitbucket importer and feature flag ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138942)
- [Adds FE banner for importing projects](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/139253)
- [Add new attribute to check codequality report status ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/139523)
- [Truncate a CI_MERGE_REQUEST_DESCRIPTION if it's too big](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/139605)
- [Tests covering the Cells demos](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/139288)
- [Adds link from model detail to latest version ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/139796)
- [Remove gitlab_routing_helper.rb: upgrade_plan_path 🤝✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140404)
- [Update GooglePlay integration docs ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140670)
- [Refactor group settings menu items ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140665/diffs)
- [Remove the unused cache key param from encoding ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140704)

## Issues raised

- [ImportFailure records created during file import should include IID in `external_identifiers` where possible](https://gitlab.com/gitlab-org/gitlab/-/issues/434945)

## Other

- Helping to refine [Import chosen relation between two GitLab projects with NDJSON file](https://gitlab.com/gitlab-org/gitlab/-/issues/425798)
