# April 2024

## Code reviews

- [Frontend work for regex filter on Zoekt code search](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144625)
- [Expose "branches_to_be_notified property for Telegram integration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148281)
- [Remove the need for SaaS to configure Product Analytics](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/147833)
- [Add Google Artifact Registry count to Service Ping](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148414)

## Issues raised

- [LogCursor daemon quietly fails if event processing class no longer exists](https://gitlab.com/gitlab-org/gitlab/-/issues/454062)

## Other

Now that [I'm a maintainer 🎉](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/133613) I won't be updating this log with everything I do. I might still write up interesting things here, when the mood takes me or there's something I want to share with my team.
