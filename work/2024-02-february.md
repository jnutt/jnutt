# February 2024

## Merge requests

- [Remove BulkImports::StuckImportWorker](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143806)
- [Remove cron schedule for removed job](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145248)
- [Update Sidekiq docs for scheduled worker removal](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145261)
- [Bump Redis version used to migration checker to match production](https://gitlab.com/gitlab-org/database-team/gitlab-com-database-testing/-/merge_requests/202)
- [Remove cron schedule for removed job](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145938)
- [Update Worker removal doc](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146088)

## Reviews

- [Change logic of oauth_authorization_path for GitLab Jira App ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143543)
- [Fix MockCI REST API ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143634)
- [Fix wrong reason for error service object 🤝✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143834)
- [Add new integration logos ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143945)
- [Cleanup ignore_columns from GeoNodeStatus ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143963)
- [Jira integration - unable to configure vulnerabilities 💡](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144023)
  - George spotted an undercoverage issue I had missed.
- [Feature spec for security policies scan finding rule ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143147)
  - Had some trouble with this one locally, but ended up finding a test flake and suggested a minor restructuring to the tests.
- [Adds attribution_notice to Bugzilla integration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144166)
- [Fix corpus management empty state ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144192)
  - Mostly a frontend change.
- [Show private group invited members to shared group members ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143651)
  - One of the more complex recent MRs in terms of grokking the domain.
- [Add application setting column for downstream pipeline limit ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144077)
  - A big MR, but I was just looking at the tests.
- [Show private invited group members to shared project members](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144446)
- [Drop product_analytics_events_experimental table ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144626)
- [Formalise best practice around flag rollouts](https://gitlab.com/gitlab-com/content-sites/handbook/-/merge_requests/3627)
- [Add desired sharding keys for importers ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144856)
- [Add namespace to Github Importer messages](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144888)
  - I'm still getting to grips with our i18n strategy and how to balace flexibility with de-duplication.
- [Add missing sharding keys ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144667)
- [Unquarantine bulk_create_scan_result_policy test ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145384)
- [Add safe rel attribute on links with target "_blank" ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145516)
- [Remove cron schedule for removed job](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145633)
  - This one sort of fractalled out into another MR and a docs change.
- [Remove js- prefix from approvals table ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145236)
- [Update dependency grape-entity to '~> 0.10.2' ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145588)
- [Update dependency grape-swagger-entity to '~> 0.5.3' ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145602)
- [Security MR 🔒](https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/3898)
  - The maintainer went into a slightly deeper dive as to the desired behaviour, as opposed to just making sure the change worked.
- [Fix rubocop todos ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145917)
- [Make GitGuardian integration group/instance level ✨](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146090)
- [Added Google Cloud IAM logo](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146186)
  - There was a merge conflict I maybe should have spotted. I'm not sure if it was present when I reviewed.

## Issues raised

- [Update swagger-ui-dist version in SwaggerUI GitLab Pages CI template](https://gitlab.com/gitlab-org/gitlab/-/issues/440389)
