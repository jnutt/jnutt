# August 2023

## MRs

- [Remove broken link to average equipment costs](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/merge_requests/2341)
- [Update Secure Code Warrior guidance](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/merge_requests/2342)
- [Update team info for jnutt](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/128698)
- [Update WebHookLog URL sanitisation to support masked segments](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/130664)


## Other

My first few weeks at GitLab! 🎉 The vast majority of this time was spent on onboarding tasks and getting to know people, but I did start working towards my initial contributions.
